import React, { Component } from 'react';
import Dijkstra from './components/Dijkstra';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'

class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
        <h1>Graph Dijkstra</h1>
          <img src={logo} className="App-logo" alt="logo" />
            <Dijkstra />
        </header>
      </div>
    );
  }
}

export default App;
