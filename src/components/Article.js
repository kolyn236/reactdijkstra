import React, {Component} from 'react'

class Article extends Component {

    // constructor(props){

    //     super(props)

    //     this.state = {
    //         isOpen : true
    //     }

    // }
    
    state = {
        isOpen: this.props.defaultOpen
    }

    componentWillMount(){

    }

    shouldComponentUpdate(nextProps, nextState) {

        return this.state.isOpen !== nextState.isOpen
    }

    componentWillReceiveProps(nextProps){

        if(nextProps.defaultOpen !== this.props.defaultOpen){

            this.setState({
                isOpen: nextProps.defaultOpen
            })
        }

    }

    render(){
        const {article} = this.props
        const body = this.state.isOpen && <section>{article.text}</section>
    

        return (
         <div className="container">
            <div className="card">
                <div className='card-header'>
                    <button style={{float:'right'}} className="btn btn-primary" onClick={this.handleClick}>
                        {this.state.isOpen ? 'Close' : 'Show' }
                    </button>
                    
                    <h2 style={{color:"black"}}>{article.title}</h2>
                </div>
                
                <div className='card-body'>
                    <span style={{color:'black'}}>{body}</span>
                    <h3 style={{fontWeight:'lighter', float:'left'}}>creaction date: {(new Date()).toDateString()}</h3>
                </div>
                
                
            </div>  
         </div>   
        )

    }

    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }


}

export default Article