export default [
    {
        "id": 1,
        "hidden": true,
        "text": "orem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, u",
        "title": "First post script",
        "color": "#82b92c",
        "number": 123,
        "string": "Hello World"
    },
    {
        "id": 2,
        "hidden": true,
        "text": "React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.        Declarative views make your code more predictable and easier to debug.",
        "title": "Second post script",
        "color": "#82b92c",
        "number": 1234,
        "string": "ololoworld"
    },

]